//
//  NewSchoolViewController.swift
//  ProgrammingApp
//
//  Created by student on 3/12/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {
    
   
    
    
    
    @IBOutlet weak var nameTF: UITextField!
    
    @IBOutlet weak var coachTF: UITextField!
    var newSchool: School!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func done(_ sender: Any) {
        let schoolName = nameTF.text!
        let coachName = coachTF.text!
        Schools.shared.add(school: School(name: schoolName, coach: coachName))
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
