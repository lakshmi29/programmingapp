//
//  NewTeamViewController.swift
//  ProgrammingApp
//
//  Created by student on 3/12/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {
    
    
    @IBOutlet weak var teamNameTF: UITextField!
    
    @IBOutlet weak var student0TF: UITextField!
    
    @IBOutlet weak var student1TF: UITextField!
    
    @IBOutlet weak var student2TF: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    var school: School!
    
    @IBAction func done(_ sender: Any) {
        let teamName = teamNameTF.text!
        let student0Name = student0TF.text!
        let student1Name = student1TF.text!
        let student2Name = student2TF.text!
        school.addTeam(name: teamName, students: [student0Name, student1Name, student2Name])
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil) 
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
